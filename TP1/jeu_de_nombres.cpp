#include <iostream>

void question_1_1() {
	std::cout << "Quel est ton prénom ? : ";

	std::string prenom;
	std::cin >> prenom;

	std::cout << "Salut " << prenom << std::endl;
}

void question_1_2() {
	std::cout << "Quel est ton prénom et nom ? : ";

	std::string prenom, nom;
	std::cin >> prenom >> nom;

	prenom[0] = std::toupper(prenom[0]);
	for (size_t i = 1; i < prenom.size(); i++) {
		prenom[i] = std::tolower(prenom[i]);
	}

	for (char &c : nom)
		c = std::toupper(c);

	std::cout << "Bonjour " << prenom << " " << nom << std::endl;
}

void question_2_1_and_2_2() {
	std::cout << "Début du jeu : Le juste prix" << std::endl;
	std::cout << "Génération du prix à trouver..." << std::endl;

	srand(time(NULL));
	int prix_a_trouver = rand() % 100;
	int prix_proposer_user = 0;
	int nb_de_coups = 1;

	while (true) {
		std::cout << "Devine le prix : ";
		std::cin >> prix_proposer_user;

		if (prix_proposer_user > prix_a_trouver)
			std::cout << "C'est moins !" << std::endl;
		else if (prix_proposer_user < prix_a_trouver)
			std::cout << "C'est plus !" << std::endl;
		else {
			std::cout << "FELICITATION! Tu as trouvée le juste prix en "
					  << nb_de_coups << " coups" << std::endl;
			return;
		}

		nb_de_coups++;
	}
}

void question_2_3() {

	std::cout << "Début du jeu : Le juste prix (ordinateur qui devine)"
			  << std::endl;

	int nb_a_faire_deviner = -1;
	int valeur_min = 0;
	int valeur_max = 1000;

	char reponse_user = '\0';

	while (nb_a_faire_deviner < 0 || nb_a_faire_deviner > valeur_max) {
		std::cout << "Choisi le nombre à faire deviner (entre 0 et "
				  << valeur_max << ") : ";
		std::cin >> nb_a_faire_deviner;
	}

	while (true) {
		nb_a_faire_deviner = (valeur_min + valeur_max) / 2;
		std::cout << "(Ordi) Est-ce le nombre " << nb_a_faire_deviner
				  << " (+, - ou =) ? ";
		std::cin >> reponse_user;

		if (reponse_user == '-') {
			valeur_max = nb_a_faire_deviner - 1;
		} else if (reponse_user == '+') {
			valeur_min = nb_a_faire_deviner + 1;
		} else if (reponse_user == '=') {
			std::cout << "J'ai trouvé! Votre nombre est " << nb_a_faire_deviner
					  << std::endl;
			break;
		} else {
			std::cout << "Réponse non reconnue. Veuillez utiliser +, - ou =\n";
		}
	}
}

int main() {
	question_1_1();
	question_1_2();
	question_2_1_and_2_2();
	question_2_3();
}