#include <cstdlib>
#include <iostream>

int sum_2_numbers(int a, int b) { return a + b; }
void replace_wtih_pointers(int a, int b, int *c) { *c = sum_2_numbers(a, b); }
void replace_wtih_reference(int a, int b, int &c) { c = sum_2_numbers(a, b); }

void questions_1_and_2() {
    int nombre_a = 0;
    int nombre_b = 0;

    std::cout << "Fournir un nombre a: ";
    std::cin >> nombre_a;

    std::cout << "Fournir un nombre b: ";
    std::cin >> nombre_b;

    std::cout << "La somme de ces deux nombres est "
              << sum_2_numbers(nombre_a, nombre_b) << std::endl;

    int nombre_c = 0;
    std::cout << "Fournir un nombre c: ";
    std::cin >> nombre_c;
    std::cout << "La variable qui contient le nombre c contient : " << nombre_c
              << std::endl;
    std::cout << "Appel de la fonction replace_with_pointers..." << std::endl;
    replace_wtih_pointers(nombre_a, nombre_b, &nombre_c);
    std::cout << "La variable qui contient le nombre c contient mtn : "
              << nombre_c << std::endl;

    std::cout << "Fournir un nombre a: ";
    std::cin >> nombre_a;

    std::cout << "Fournir un nombre b: ";
    std::cin >> nombre_b;
    std::cout << "Appel de la fonction replace_with_reference..." << std::endl;
    replace_wtih_reference(nombre_a, nombre_b, nombre_c);
    std::cout << "La variable contenant la valeur c contient mtn : " << nombre_c
              << std::endl;
}

void question_3(bool isDecreasing) {
    const int NB_INT = 50;
    int tabs_int[NB_INT];

    std::cout << "Génération d'un tableau de contenant des entiers aléatoire..."
              << std::endl;

    srand(time(NULL));
    for (int i = 0; i < NB_INT; i++) {
        tabs_int[i] = rand() % 1000;
    }

    std::cout << "Trie du tableau en cours..." << std::endl;
	// version notation référence
    // for (int i = NB_INT; i > 0; i--) {
    //     for (int j = 0, c = 0; j < i - 1; j++) {
    //         if (tabs_int[j + 1] < tabs_int[j]) {
    //             c = tabs_int[j + 1];
    //             tabs_int[j + 1] = tabs_int[j];
    //             tabs_int[j] = c;
    //         }
    //     }
    // }

    // version notation pointeur
    for (int i = NB_INT; i > 0; i--) {
        for (int j = 0, c = 0; j < i - 1; j++) {
            if (*(tabs_int + j + 1) < *(tabs_int + j)) {
                c = *(tabs_int + j + 1);
                *(tabs_int + j + 1) = *(tabs_int + j);
                *(tabs_int + j) = c;
            }
        }
    }

    if (isDecreasing) {
        int tabs_int_decreasing[NB_INT] = {0};
        for (int i = NB_INT - 1, j = 0; i >= 0; i--, j++)
            tabs_int_decreasing[j] = tabs_int[i];

        std::cout << "Affichage du tableau trié (decroissant) : " << std::endl;
        for (int i = 0; i < NB_INT; i++)
            std::cout << tabs_int_decreasing[i] << "\n";
    } else {
        std::cout << "Affichage du tableau trié (croissant) : " << std::endl;
        for (int i = 0; i < NB_INT; i++)
            std::cout << tabs_int[i] << "\n";
    }
}

int main(int ac, char **av) {
    // questions_1_and_2();

    if (!av[1])
        question_3(false);
    else
        question_3(true);
}