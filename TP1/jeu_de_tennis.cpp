#include <iostream>

int find_end_buffer(char *buffer) {
	int i = 0;
	while (*buffer++) {
		i++;
	}

	return i;
}

std::string determiner_score(int point_joueur1, int point_joueur2) {
	int scores_possible[] = {0, 15, 30, 40};

	char buffer[100] = {0};

	if (point_joueur1 > 3 || point_joueur2 > 3) {
		int diff = point_joueur1 - point_joueur2;

		switch (diff) {
		case 0:
			sprintf(buffer, "=> égalité, il faut continuer");
			break;

		case 1:
			sprintf(buffer, "=> avantage joueur 1");
			break;

		case -1:
			sprintf(buffer, "=> avantage joueur 2");
			break;

		case 2:
			sprintf(buffer, "=> joueur 1 gagne le jeu");
			break;

		case -2:
			sprintf(buffer, "=> joueur 2 gagne le jeu");
			break;

		default:
			sprintf(buffer, "Impossible");
		}

		return buffer;
	}

	if (point_joueur1 == point_joueur2) {
		sprintf(buffer, "%d - %d => égalité, il faut continuer",
				scores_possible[point_joueur1], scores_possible[point_joueur2]);

		return buffer;
	}

	int score_joueur1 = scores_possible[point_joueur1];
	int score_joueur2 = scores_possible[point_joueur2];

	if (score_joueur1 == 40) {
		sprintf(buffer, "=> joueur 1 gagne");
	} else if (score_joueur2 == 40) {
		sprintf(buffer, "=> joueur 2 gagne");
	}

	int current_end_buffer = find_end_buffer(buffer);
	buffer[current_end_buffer] = ' ';
	sprintf(buffer + current_end_buffer + 1, "%d - %d", score_joueur1,
			score_joueur2);

	return buffer;
}

int main(int ac, char **av) {
	if (ac < 3 || ac > 3) {
		printf("usage:\n\t./jeu_de_tennis point_joueur1 point_joueur2\n");
		return -1;
	}

	std::cout << determiner_score(atoi(av[1]), atoi(av[2])) << std::endl;

	return 0;
}
