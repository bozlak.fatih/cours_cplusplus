#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include "grille.h"
struct GrillePuissance4 : public Grille
{
public:
    GrillePuissance4();

    const int NB_LIGNE = 4;
    const int NB_COLONNE = 7;
    const char CASE_DEFAUT = '.';

    char tableau[4][7];
    void rajouterPion(char symbole , int colonneParam);
    void afficherGrille()override;
    void initialiserGrille();
    bool estCaseLibre(int indiceLigne , int indiceColonne);
    bool estCaseExiste(int indiceLigne , int indiceColonne);
    bool estGagne(char symboleDernierPion);
    bool estNulle();
};

#endif // GRILLEPUISSANCE4_H
