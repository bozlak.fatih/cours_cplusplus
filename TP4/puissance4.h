#ifndef PUISSANCE4_H
#define PUISSANCE4_H

#include "jeu.h"

class Puissance4 : private Jeu
{
public:
    Puissance4();
    void lancerJeu();
};

#endif // PUISSANCE4_H
