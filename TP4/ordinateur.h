#ifndef ORDINATEUR_H
#define ORDINATEUR_H
#include "participant.h"
#include <string>

class Ordinateur : public Participant
{
public:
    Ordinateur();
    std::string const getNom();
    Pion poserPionLigneColonne(const int nbLigneMax , const int nbColonneMax);
    Pion poserPionColonne(const int nbColonneMax);
    char getSymbole();
private :
    const std::string NAME = "Arnold";
};

#endif // ORDINATEUR_H
