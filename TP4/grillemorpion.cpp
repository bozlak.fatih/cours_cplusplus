#include "grillemorpion.h"
#include "grille.h"
#include <vector>
#include <iostream>

GrilleMorpion::GrilleMorpion()  :  Grille ()
{

}

void GrilleMorpion::rajouterPion(char symbole, int ligneParam, int colonneParam)
{  
    tableau[ligneParam][colonneParam] = symbole;
}

void GrilleMorpion::initialiserGrille()
{
    for (int i = 0; i != this->NB_LIGNE; i++) {
            for (int j = 0; j != this->NB_COLONNE; j++) {
                tableau[i][j] = CASE_DEFAUT;
            }
    }
}

void GrilleMorpion::afficherGrille()
{
    for (int i = 0; i != this->NB_LIGNE; i++) {
            for (int j = 0; j != this->NB_COLONNE; j++) {
                std::cout << tableau[i][j];
            }
            std::cout << std::endl;
    }
}

bool GrilleMorpion::estCaseExiste(int indiceLigne , int indiceColonne)
{
    if (indiceLigne >= this->NB_LIGNE || indiceColonne >= this->NB_COLONNE)
    {
        return false;
    }
    return true;
}

bool GrilleMorpion::estCaseLibre(int indiceLigne , int indiceColonne)
{

    if (tableau[indiceLigne][indiceColonne]!= this->CASE_DEFAUT){
        return false;
    }
    return true;
}


bool GrilleMorpion::estGagne(char symboleDernierPion)
{
    bool case00 = (tableau[0][0]==symboleDernierPion);
    bool case01 = (tableau[0][1]==symboleDernierPion);
    bool case02 = (tableau[0][2]==symboleDernierPion);
    bool case10 = (tableau[1][0]==symboleDernierPion);
    bool case11 = (tableau[1][1]==symboleDernierPion);
    bool case12 = (tableau[1][2]==symboleDernierPion);
    bool case20 = (tableau[2][0]==symboleDernierPion);
    bool case21 = (tableau[2][1]==symboleDernierPion);
    bool case22 = (tableau[2][2]==symboleDernierPion);

    bool ligne0estGagne = (case00&&case01 && case02);
    bool ligne1estGagne = (case10&&case11 && case12);
    bool ligne2estGagne = (case20&& case21 && case22);
    bool colonne0estGagne = (case00&&case10 && case20);
    bool colonne1estGagne = (case01&&case11 && case21);
    bool colonne2estGagne = (case02&&case12 && case22);
    bool diagonale1estGagne = (case00 && case11 && case22 );
    bool diagonale2estGagne = (case02 && case11 && case20 );

    bool estGagne = (ligne0estGagne||ligne1estGagne||ligne2estGagne||
                     colonne0estGagne||colonne1estGagne||colonne2estGagne||
                     diagonale1estGagne||diagonale2estGagne);

    return estGagne ;
}

bool GrilleMorpion::estNulle()
{
    for (int i = 0; i != this->NB_LIGNE; i++) {
        for (int j = 0; j != this->NB_COLONNE; j++) {
            if( tableau[i][j] == CASE_DEFAUT)
            {
                return false;
            }
        }
    }
    return true ;
}




