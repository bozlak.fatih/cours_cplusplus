#include "pion.h"
#include <iostream>

Pion::Pion(int xParam , int yParam, char symboleParam)
{
    this->coordonneX = xParam;
    this->coordonneY = yParam;
    this->symbole = symboleParam;

}
void Pion::SetCoordonneX(int valueX)
{
    this->coordonneX = valueX;
}

void Pion::SetCoordonneY(int valueY)
{
    this->coordonneY = valueY;
}

int Pion::GetCoordonneX()
{
    return coordonneX;
}

int Pion::GetCoordonneY()
{
    return coordonneY;
}

char Pion::GetSymbole()
{
    return symbole;
}

void Pion::setSymbole(char valueSymbole)
{
    this->symbole = valueSymbole;
}

void Pion::Afficher()
{
    std::cout<<"GetCoordonneX() renvoie la coordonnes sur l'axe des X du Pion\n";
    std::cout<<"GetCoordonneY() renvoie la coordonnes sur l'axe des Y du Pion\n";
}
