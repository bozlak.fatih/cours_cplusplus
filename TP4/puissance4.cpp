#include "puissance4.h"
#include "grillepuissance4.h"
#include "pion.h"
#include "joueur.h"
#include "ordinateur.h"

#include <iostream>
#include <string>
#include <list>
Puissance4::Puissance4()
{

}


void Puissance4::lancerJeu()
{
    std::cout<< "Bienvenue dans le jeu du Puissance 4 \n";
    int choix;
    do
    {

        std::cout<< "1-Lancer une partie"<<std::endl;
        std::cout<< "2-Revenir au menu"<<std::endl;
        std::cin>>choix;

        while(choix==1){
            GrillePuissance4 plateau = GrillePuissance4();
            Participant* listeParticipant[2];

            std::cout<< "Commençons par créer les joueurs !\n";

            std::cout<< "Veuillez saisir le pseudo  du joueur 1\n";
            std::string pseudo1;
            std::cin>>pseudo1;
            std::cout<< "Veuillez saisir le symbole (charactere unique) du joueur 1\n";
            char symbole1;
            std::cin>>symbole1;
            Joueur j1 = Joueur(symbole1,pseudo1);
            listeParticipant[0] = &j1;

            char choixJoueurs ;
            std::cout<< "Souhaitez vous jouer à 2 joueurs ? (y/n)"<<std::endl;
            std::cin>>choixJoueurs;
            if(choixJoueurs == 'y')
            {
                std::cout<< "Veuillez saisir le pseudo du joueur 2\n";
                std::string pseudo2;
                std::cin>>pseudo2;
                std::cout<< "Veuillez saisir le symbole (charactère unique) du joueur 2\n";
                char symbole2;
                std::cin>>symbole2;
                Joueur j2 = Joueur(symbole2,pseudo2);
                listeParticipant[1] = &j2;
            }
            else
            {
                Ordinateur j2 = Ordinateur();
                listeParticipant[1] = &j2;
            }

            Pion dernierPion (0,0,' ');
            bool estExiste ;
            bool estLibre;
            bool estGagne ;
            bool estNulle;

            plateau.initialiserGrille();
            std::cout<<"Début de partie !"<<std::endl;
            do
            {
                plateau.initialiserGrille();
                do
                {
                    for(int i = 0; i < 2; i++)
                    {
                        plateau.afficherGrille();
                        do{
                            std::cout<< listeParticipant[i]->getNom()<<" choisissez l'emplacement de votre pion !"<<std::endl;
                            dernierPion = listeParticipant[i]->poserPionColonne(plateau.NB_COLONNE);

                            estExiste = plateau.estCaseExiste(dernierPion.GetCoordonneX(), dernierPion.GetCoordonneY());
                            estLibre = plateau.estCaseLibre(dernierPion.GetCoordonneX(), dernierPion.GetCoordonneY());

                            if(estLibre && estExiste)
                            {
                                plateau.rajouterPion(listeParticipant[i]->getSymbole(),dernierPion.GetCoordonneY());
                            }
                            else{
                                 std::cout<<"La case doit être comprise dans le tableau et être libre"<<std::endl  ;
                            }
                        }while(!(estLibre)||!(estExiste));
                        estGagne = plateau.estGagne(dernierPion.GetSymbole());
                        estNulle = plateau.estNulle();
                        if (estGagne)
                        {
                            plateau.afficherGrille();
                            std::cout<<listeParticipant[i]->getNom()<< " a gagné !! Félicitations !\n";
                            break;
                        }
                        if(estNulle)
                        {
                            plateau.afficherGrille();
                            std::cout<<"Egalité ! Beau Match !\n";
                            break;
                        }
                    }
                }while(!estGagne && !estNulle);
                std::cout<< "1-Refaire une partie"<<std::endl;
                std::cout<< "2-Revenir au menu"<<std::endl;
                std::cin>>choix;
            }while(choix ==1);
        }
    } while(choix ==1);
}
