#ifndef MORPION_H
#define MORPION_H

#include "jeu.h"

class Morpion : private Jeu
{
public:
    Morpion();
    void lancerJeu();
};

#endif // MORPION_H
