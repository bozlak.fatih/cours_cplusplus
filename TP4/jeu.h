#ifndef JEU_H
#define JEU_H
#include <iostream>
#include <string>

class Jeu
{
public:
    Jeu();
    virtual void lancerJeu()=0;
};

#endif // JEU_H
