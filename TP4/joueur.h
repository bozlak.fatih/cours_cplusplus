#ifndef JOUEUR_H
#define JOUEUR_H

#include <iostream>
#include <string>
#include "participant.h"
#include "pion.h"

class Joueur : public Participant
{
public:
    Joueur(char symboleParam, std::string pseudoParam);
    std::string const getNom();
    char getSymbole();
    Pion poserPionLigneColonne(const int nbLigneMax , const int nbColonneMax);
    Pion poserPionColonne(const int nbColonneMax);
private :
    std::string PSEUDO;

};

#endif // JOUEUR_H
