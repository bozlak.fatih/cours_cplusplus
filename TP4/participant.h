#ifndef PARTICIPANT_H
#define PARTICIPANT_H

#include "pion.h"
#include <string>
class Participant
{
public:
    Participant(char symbole);
    virtual Pion poserPionLigneColonne(const int nbLigneMax , const int nbColonneMax)=0;
    virtual Pion poserPionColonne(const int nbColonneMax)=0;
    char getSymbole();
    virtual std::string const getNom()=0;
protected:
    const char SYMBOLE;
};

#endif // PARTICIPANT_H
