#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include "grille.h"
#include "pion.h"
#include <iostream>
#include <vector>
#include <string>

struct GrilleMorpion : public Grille
{
public:
    GrilleMorpion() ;

    const int NB_LIGNE = 3;
    const int NB_COLONNE = 3;
    const char CASE_DEFAUT = '.';

    char tableau[3][3];
    void rajouterPion(char symbole , int ligneParam , int colonneParam) ;
    void afficherGrille() override;
    void initialiserGrille();
    bool estCaseLibre(int indiceLigne , int indiceColonne);
    bool estCaseExiste(int indiceLigne , int indiceColonne);
    bool estGagne(char symboleDernierPion);
    bool estNulle();
};

#endif // GRILLEMORPION_H
