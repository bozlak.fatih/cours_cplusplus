#ifndef PION_H
#define PION_H

struct Pion
{
public:
    Pion(int xParam , int yParam, char symboleParam);
    int coordonneX ;
    int coordonneY ;
    char symbole;

    int GetCoordonneX();
    void SetCoordonneX(int valueX);
    int GetCoordonneY();
    void SetCoordonneY(int valueY);
    char GetSymbole();
    void setSymbole(char valueSymbole);
    void Afficher();
};

#endif // PION_H
