TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    grillemorpion.cpp \
    grillepuissance4.cpp \
    jeu.cpp \
        main.cpp\
        grille.cpp\
    menu.cpp \
    morpion.cpp \
    ordinateur.cpp \
        participant.cpp\
        joueur.cpp\
    pion.cpp \
    puissance4.cpp

HEADERS += \
    grille.h \
    grillemorpion.h \
    grillepuissance4.h \
    jeu.h \
    menu.h \
    morpion.h \
    ordinateur.h \
    participant.h\
    joueur.h\
    pion.h \
    puissance4.h
