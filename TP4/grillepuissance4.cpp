#include "grillepuissance4.h"
#include "iostream"

GrillePuissance4::GrillePuissance4()
{

}

void GrillePuissance4::rajouterPion(char symbole, int colonneParam)
{
    for(int i = this->NB_LIGNE-1 ; i >=0 ; i--)
    {
        if(estCaseLibre(i,colonneParam))
        {
            tableau[i][colonneParam] = symbole ;
            break;
        }
    }
}

void GrillePuissance4::initialiserGrille()
{
    for (int i = 0; i != this->NB_LIGNE; i++) {
            for (int j = 0; j != this->NB_COLONNE; j++) {
                tableau[i][j] = CASE_DEFAUT;
            }
    }
}

void GrillePuissance4::afficherGrille()
{
    for (int i = 0; i != this->NB_LIGNE; i++) {
            for (int j = 0; j != this->NB_COLONNE; j++) {
                std::cout << tableau[i][j];
            }
            std::cout << std::endl;
    }
}

bool GrillePuissance4::estCaseExiste(int indiceLigne , int indiceColonne)
{
    if (indiceLigne >= this->NB_LIGNE || indiceColonne >= this->NB_COLONNE)
    {
        return false;
    }
    return true;
}

bool GrillePuissance4::estCaseLibre(int indiceLigne , int indiceColonne)
{

    if (tableau[indiceLigne][indiceColonne]!= this->CASE_DEFAUT){
        return false;
    }
    return true;
}

bool GrillePuissance4::estGagne(char symboleDernierPion)
{
    //Verification Horizontale
    for (int i = 0; i < this->NB_LIGNE; i++) {
        for (int j = 0; j < this->NB_COLONNE - 3; j++) {
            if (tableau[i][j] == symboleDernierPion &&
                    tableau[i][j+1] == symboleDernierPion &&
                    tableau[i][j+2] == symboleDernierPion &&
                    tableau[i][j+3] == symboleDernierPion) {
                return true;
            }
        }
    }

    // Verification Verticale
    for (int i = 0; i < this->NB_LIGNE - 3; i++) {
        for (int j = 0; j < this->NB_COLONNE; j++) {
            if (tableau[i][j] == symboleDernierPion &&
                    tableau[i+1][j] == symboleDernierPion &&
                    tableau[i+2][j] == symboleDernierPion &&
                    tableau[i+3][j] == symboleDernierPion) {
                return true;
            }
        }
    }

    // Verification Deagonale Heut gauche bas droite
    for (int i = 0; i < this->NB_LIGNE - 3; i++) {
        for (int j = 0; j < this->NB_COLONNE - 3; j++) {
            if (tableau[i][j] == symboleDernierPion &&
                    tableau[i+1][j+1] == symboleDernierPion &&
                    tableau[i+2][j+2] == symboleDernierPion &&
                    tableau[i+3][j+3] == symboleDernierPion) {
                return true;
            }
        }
    }

    // Verification Diagonale Haut droite bas gauche
    for (int i = 0; i < this->NB_LIGNE - 3; i++) {
        for (int j = 3; j < this->NB_COLONNE; j++) {
            if (tableau[i][j] == symboleDernierPion &&
                    tableau[i+1][j-1] == symboleDernierPion &&
                    tableau[i+2][j-2] == symboleDernierPion &&
                    tableau[i+3][j-3] == symboleDernierPion) {
                return true;
            }
        }
    }
    return false;
}

bool GrillePuissance4::estNulle()
{
    for (int i = 0; i != this->NB_LIGNE; i++) {
        for (int j = 0; j != this->NB_COLONNE; j++) {
            if( tableau[i][j] == CASE_DEFAUT)
            {
                return false;
            }
        }
    }
    return true ;
}
