#include "ordinateur.h"
#include <cstdlib>

Ordinateur::Ordinateur(): Participant('@')
{

}


std::string const Ordinateur::getNom()
{
    return this->NAME;
}

Pion Ordinateur::poserPionLigneColonne(const int nbLigneMax, const int nbColonneMax)
{
    int lignePion =rand() % nbLigneMax;
    int colonnePion=rand() % nbColonneMax;
    return Pion(lignePion,colonnePion,Participant::getSymbole());
}

Pion Ordinateur::poserPionColonne(const int nbColonneMax)
{
    int lignePion =0;
    int colonnePion=rand() % nbColonneMax;
    return Pion(lignePion,colonnePion,Participant::getSymbole());
}
