#include "joueur.h"
#include <string>
Joueur::Joueur(char symboleParam, std::string pseudoParam): Participant(symboleParam) , PSEUDO(pseudoParam)
{

}

std::string const Joueur::getNom()
{
    return this->PSEUDO;
}


Pion Joueur::poserPionLigneColonne(const int nbLigneMax , const int nbColonneMax)
{
    int lignePion;
    int colonnePion;
    do{
    std::cout<<"Sélectionner l'indice de la ligne entre 0 et " << nbLigneMax-1 <<std::endl;
    std::cin>>lignePion;
    }while(lignePion>nbLigneMax || lignePion<0);

    do{
    std::cout<<"Sélectionner l'indice de la colonne entre 0 et " << nbColonneMax-1 <<std::endl;
    std::cin>>colonnePion;
    }while(colonnePion>nbColonneMax || colonnePion<0);

    return Pion(lignePion,colonnePion,Participant::getSymbole());
}

Pion Joueur::poserPionColonne( const int nbColonneMax)
{
    int lignePion = 0;
    int colonnePion;

    do{
    std::cout<<"Sélectionner l'indice de la colonne entre 0 et " << nbColonneMax-1 <<std::endl;
    std::cin>>colonnePion;
    }while(colonnePion>nbColonneMax || colonnePion<0);

    return Pion(lignePion,colonnePion,Participant::getSymbole());
}
