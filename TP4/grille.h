#ifndef GRILLE_H
#define GRILLE_H


struct Grille
{
public:
    Grille();

    virtual void afficherGrille() =0;

};

#endif // GRILLE_H
