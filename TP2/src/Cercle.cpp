//
// Created by Fatih BOZLAK on 24/10/2023.
//

#include <cmath>
#include <iostream>
#include "Cercle.h"

Cercle::Cercle(Point &centre, int diametre) : \
    centre(centre), diametre(diametre) {};

Point Cercle::getCentre() const {
    return this->centre;
}

void Cercle::setCentre(Point &point) {
    this->centre = point;
}

int Cercle::getDiametre() const {
    return this->diametre;
}

void Cercle::setDiametre(int diametre) {
    this->diametre = diametre;
}

double Cercle::calculerPerimetre() const {
    return 2 * M_PI * (this->diametre / 2.0);
}

double Cercle::calculerSurface() const {
    double rayon = (this->diametre / 2.0);
    return (rayon * rayon) * M_PI;
}

bool Cercle::estSurLeCercle(Point &point) const {
    const double EPSILON = 0.01;

    double distancePointAuCentre = std::sqrt(
            std::pow(point.x - this->centre.x, 2) + std::pow(point.y - this->centre.y, 2));

    if (std::abs(distancePointAuCentre - this->diametre / 2.0) < EPSILON)
        return true;

    return false;
}

bool Cercle::estDansLeCercle(Point &point) const {
    double distancePointAuCentre = std::sqrt(
            std::pow(point.x - this->centre.x, 2) + std::pow(point.y - this->centre.y, 2));
    
    if (distancePointAuCentre < (this->diametre / 2.0))
        return true;

    return false;
}