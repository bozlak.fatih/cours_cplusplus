//
// Created by Fatih BOZLAK on 24/10/2023.
//

#include <cmath>
#include <iostream>
#include "Triangle.h"

Triangle::Triangle(Point p1, Point p2, Point p3) : \
    p1(p1), p2(p2), p3(p3) {}

Point Triangle::getP1() {
    return this->p1;
}

void Triangle::setP1(Point p1) {
    this->p1 = p1;
}

Point Triangle::getP2() {
    return this->p2;
}

void Triangle::setP2(Point p2) {
    this->p2 = p2;
}

Point Triangle::getP3() {
    return this->p3;
}

void Triangle::setP3(Point p3) {
    this->p3 = p3;
}

double Triangle::distanceEntrePoint(Point point1, Point point2) {
    return std::sqrt(std::pow(point2.x - point1.x, 2) + std::pow(point2.y - point1.y, 2));
}

std::pair<Point, Point> Triangle::getBase() {
    std::pair<Point, Point> segments[3] = {std::pair(p1, p2),
                                           std::pair(p2, p3),
                                           std::pair(p3, p1)};

    double tabDistances[3] = {0};
    int iMaxDistance = 0;

    tabDistances[0] = Triangle::distanceEntrePoint(p1, p2);
    tabDistances[1] = Triangle::distanceEntrePoint(p2, p3);
    tabDistances[2] = Triangle::distanceEntrePoint(p3, p1);


    for (int i = 0; i < 3; i++) {
        if (tabDistances[i] >= tabDistances[iMaxDistance]) {
            iMaxDistance = i;
        }
    }

    return segments[iMaxDistance];
}

double Triangle::getSurface() {
    // Utilisation de la formule de Heron pour trouver l'aire A du triangle
    double semiPerimetre = (
                                   Triangle::distanceEntrePoint(p1, p2) +
                                   Triangle::distanceEntrePoint(p2, p3) +
                                   Triangle::distanceEntrePoint(p3, p1)
                           ) / 2;

    double aire = std::sqrt(
            semiPerimetre *
            (semiPerimetre - Triangle::distanceEntrePoint(p1, p2)) *
            (semiPerimetre - Triangle::distanceEntrePoint(p2, p3)) *
            (semiPerimetre - Triangle::distanceEntrePoint(p3, p1))
    );

    return aire;
}

double Triangle::getHauteur() {
    std::pair<Point, Point> baseConsideree = this->getBase();
    printf("La base considirée est le segment du point (x=%f, y=%f) et point (x=%f, y=%f)\n", baseConsideree.first.x,
           baseConsideree.first.y,
           baseConsideree.second.x, baseConsideree.second.y);

    double aire = this->getSurface();

    return (2 * aire) / Triangle::distanceEntrePoint(baseConsideree.first, baseConsideree.second);
}

void Triangle::getLongueurs() {
    printf("Le segment de p1 à p2 vaut %f\n", Triangle::distanceEntrePoint(p1, p2));
    printf("Le segment de p2 à p3 vaut %f\n", Triangle::distanceEntrePoint(p2, p3));
    printf("Le segment de p3 à p1 vaut %f\n", Triangle::distanceEntrePoint(p3, p1));
}

bool Triangle::isEquilateral() {
    double longueurP1toP2 = Triangle::distanceEntrePoint(p1, p2);
    double longueurP2toP3 = Triangle::distanceEntrePoint(p2, p3);
    double longueurP3toP1 = Triangle::distanceEntrePoint(p3, p1);

    return (longueurP1toP2 == longueurP2toP3) && (longueurP2toP3 == longueurP3toP1);
}

// très fortément inspiré d'internet.
bool Triangle::isRectangle() {
    double longueurP1toP2 = distanceEntrePoint(p1, p2);
    double longueurP2toP3 = distanceEntrePoint(p2, p3);
    double longueurP3toP1 = distanceEntrePoint(p3, p1);

    // Vérifiez si c'est un triangle rectangle en utilisant le théorème de Pythagore.
    bool estRectangle =
            (longueurP1toP2 * longueurP1toP2 == longueurP2toP3 * longueurP2toP3 + longueurP3toP1 * longueurP3toP1) ||
            (longueurP2toP3 * longueurP2toP3 == longueurP1toP2 * longueurP1toP2 + longueurP3toP1 * longueurP3toP1) ||
            (longueurP3toP1 * longueurP3toP1 == longueurP1toP2 * longueurP1toP2 + longueurP2toP3 * longueurP2toP3);

    // Si c'est un triangle rectangle, vérifiez ensuite s'il est isocèle.
    if (estRectangle) {
        return (longueurP1toP2 == longueurP2toP3) ||
               (longueurP2toP3 == longueurP3toP1) ||
               (longueurP3toP1 == longueurP1toP2);
    }

    return false;
}








