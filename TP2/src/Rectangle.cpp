//
// Created by Fatih BOZLAK on 22/10/2023.
//

#include "Rectangle.h"

Rectangle::Rectangle(int longueur, int largeur, Point &coinGauche) : \
        longueur(longueur), largeur(largeur), coinGauche(coinGauche) {}

int Rectangle::getLongueur() const {
    return this->longueur;
}

void Rectangle::setLongueur(int longueur) {
    this->longueur = longueur;
}

int Rectangle::getLargeur() const {
    return this->largeur;
}

void Rectangle::setLargeur(int largeur) {
    this->largeur = largeur;
}

Point Rectangle::getCoinGauche() const {
    return this->coinGauche;
}

void Rectangle::setCoinGauche(Point &coinGauche) {
    this->coinGauche = coinGauche;
}

int Rectangle::calculerPerimetre() const {
    return (this->longueur + this->largeur) * 2;
}

int Rectangle::calculerSurface() const {
    return (this->longueur * this->largeur);
}

bool Rectangle::aUnPerimetrePlusGrand(Rectangle &autre) const {
    if (this->calculerPerimetre() > autre.calculerPerimetre())
        return true;

    return false;
}

bool Rectangle::aUnePlusGrandeSurface(Rectangle &autre) const {
    if (this->calculerSurface() > autre.calculerSurface())
        return true;

    return false;
}
