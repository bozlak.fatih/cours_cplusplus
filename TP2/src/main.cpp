#include <iostream>
#include "Point.h"
#include "Rectangle.h"
#include "Cercle.h"
#include "Triangle.h"
#include "Carre.h"

#define SUCESS 1
#define FAILURE 0
#define TEST_VALUE int

using namespace std;

void message(string messageAAfficher, bool retourLigne = true) {
    if (retourLigne)
        cout << messageAAfficher << endl;
    else
        cout << messageAAfficher;
}

TEST_VALUE test_point_h() {
    cout << "=== TEST POINT.H ===" << endl;

    struct Point p1 = {10.5, 20.5};
    cout << "- Création d'un point de coordonnées x=10.5 et y=20.5 : OK" << endl;

    if (p1.x != 10.5 && p1.y != 20.5) {
        cout << "Problème dans la création du point" << endl;
        return FAILURE;
    }

    cout << "- Affichage de x et y : " << p1.x << " " << p1.y << endl;

    cout << "=== FIN TEST POINT.H ===" << endl;

    return SUCESS;
}

TEST_VALUE test_rectangle_h() {
    message("=== TEST RECTANGLE.H ===");

    const int LONGUEUR = 10;
    const int NEW_LONGUEUR = 5;
    const int LARGEUR = 5;
    const int NEW_LARGEUR = 2;

    const int NEW_PERIMETRE = 14;

    const int NEW_SURFACE = 10;

    const float X = 10.5;
    const float Y = 20.5;


    Point coinGauche = Point(X, Y);
    message("Création d'un point au coordonnées x=10.5 et y=20.5");

    Rectangle rec1 = Rectangle(LONGUEUR, LARGEUR, coinGauche);
    message("Création d'un rectangle de longueur=10, largueur=5 et coinGauche=(10.5, 20.5)");

    message("Test getLongueur : ", false);
    if (rec1.getLongueur() == LONGUEUR)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test getLargeur : ", false);
    if (rec1.getLargeur() == LARGEUR)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test setLongueur : ", false);
    rec1.setLongueur(NEW_LONGUEUR);
    if (rec1.getLongueur() == NEW_LONGUEUR)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test setLargeur : ", false);
    rec1.setLargeur(NEW_LARGEUR);
    if (rec1.getLargeur() == NEW_LARGEUR)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test calculerPerimetre : ", false);
    if (rec1.calculerPerimetre() == NEW_PERIMETRE)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test calculerSurface : ", false);
    if (rec1.calculerSurface() == NEW_SURFACE)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    Rectangle rec2 = Rectangle(LONGUEUR, LARGEUR, coinGauche);
    message("Test aUnPerimetrePlusGrand : ", false);
    if (!rec1.aUnPerimetrePlusGrand(rec2))
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test aUneSurfacePlusGrande : ", false);
    if (!rec1.aUnePlusGrandeSurface(rec2))
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("=== FIN TEST RECTANGLE.H ===");
    return SUCESS;
}

TEST_VALUE test_cercle_h() {
    message("=== TEST CERCLE.H ===");

    const float X = 10.0;
    const float Y = 20.0;
    const int DIAMETRE = 100;
    const double PERIMETRE = 314.159;
    const double SURFACE = 7853.98;
    const double EPSILON = 0.01;

    message("Création d'un point aux coordonnées x=10.0 et y=20.0");
    Point centre = Point(X, Y);
    Point pointSurLeCercle = Point(45.35, 55.35);
    Point pointDansLeCercle = Point(31.21, 41.21);

    message("Création d'un cercle de diamètre 100 et de centre x=10.0, y=20.0");
    Cercle c1 = Cercle(centre, DIAMETRE);

    message("Test getCentre : ", false);
    if (c1.getCentre().x == centre.x && c1.getCentre().y == centre.y)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test getDiametre : ", false);
    if (c1.getDiametre() == DIAMETRE)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    /*
     * Attention la comparaison "simple ==" ne fonctionnait pas les doubles.
     * J'ai donc trouver cette technique
     */
    message("Test calculerPerimetre : ", false);
    if (std::abs(c1.calculerPerimetre() - PERIMETRE) < EPSILON)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test calculerSurface : ", false);
    if (std::abs(c1.calculerSurface() - SURFACE) < EPSILON)
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test estSurLeCercle : ", false);
    if (c1.estSurLeCercle(pointSurLeCercle))
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }

    message("Test estDansLeCercle : ", false);
    if (c1.estDansLeCercle(pointDansLeCercle))
        message(to_string(SUCESS));
    else {
        message(to_string(FAILURE));
        exit(-1);
    }


    message("=== FIN TEST CERCLE.H ===");
    return SUCESS;
}

int main() {
    test_point_h();
    test_rectangle_h();
    test_cercle_h();

    Triangle t1 = Triangle(Point(0, 0), Point(3, 0), Point(2, 2));
    std::pair<Point, Point> base = t1.getBase();

    // "obligé" d'utiliser printf, je n'ai pas réussi à utiliser std::format
    printf("La base est le segment du point (x=%f, y=%f) au point (x=%f, y=%f)\n", base.first.x, base.first.y,
           base.second.x, base.second.y);

    printf("La hauteur est de longueur %f\n", t1.getHauteur());

    t1.getLongueurs();

    printf("L'aide (surface) de ce triangle est de %f\n", t1.getSurface());

    if(t1.isEquilateral())
        printf("Le triangle est équilatéral\n");
    else
        printf("Le triangle n'est pas équilatéral\n");


    Carre carre = Carre(1, Point(1, 0));
    Carre carre2 = Carre(10, Point(10, 0));
    printf("Longueur du carré : %d\n", carre.getLongueur());
    printf("Largeur du carré : %d\n", carre.getLargeur());
    printf("Périmètre du carré : %d\n", carre.calculerPerimetre());
    printf("Surface du carré : %d\n", carre.calculerSurface());

    if(carre2.aUnePlusGrandeSurface(carre)){
        printf("Le carre2 à une plus grande surface\n");
    } else {
        printf("Le carre1 à une plus grande surface\n");
    }

    if(carre2.aUnPerimetrePlusGrand(carre)){
        printf("Le carre2 à un plus grand périmètre\n");
    } else {
        printf("Le carre1 à un plus grand périmètre\n");
    }

    return 0;
}
