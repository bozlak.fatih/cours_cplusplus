//
// Created by Fatih BOZLAK on 22/10/2023.
//

#ifndef TP2_RECTANGLE_H
#define TP2_RECTANGLE_H


#include "Point.h"

class Rectangle {
private:
    int longueur, largeur;
    Point coinGauche;

public:
    Rectangle(int longueur, int largeur, Point &coinGauche);

    int getLongueur() const;

    void setLongueur(int longueur);

    int getLargeur() const;

    void setLargeur(int largeur);

    Point getCoinGauche() const;

    void setCoinGauche(Point &coinGauche);

    int calculerPerimetre() const;

    int calculerSurface() const;

    bool aUnPerimetrePlusGrand(Rectangle &autre) const;

    bool aUnePlusGrandeSurface(Rectangle &autre) const;
};


#endif //TP2_RECTANGLE_H
