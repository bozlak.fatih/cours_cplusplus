//
// Created by Fatih BOZLAK on 24/10/2023.
//

#ifndef TP2_TRIANGLE_H
#define TP2_TRIANGLE_H


#include <utility>
#include "Point.h"

class Triangle {
private:
    Point p1, p2, p3;

    static double distanceEntrePoint(Point point1, Point point2);

public:

    Triangle(Point p1, Point p2, Point p3);

    Point getP1();

    Point getP2();

    Point getP3();

    void setP1(Point p1);

    void setP2(Point p2);

    void setP3(Point p3);

    std::pair<Point, Point> getBase();

    double getSurface();

    double getHauteur();

    void getLongueurs();

    bool isIsocele();

    bool isRectangle();

    bool isEquilateral();


};


#endif //TP2_TRIANGLE_H
