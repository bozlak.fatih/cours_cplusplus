//
// Created by Fatih BOZLAK on 24/10/2023.
//

#ifndef TP2_CERCLE_H
#define TP2_CERCLE_H

#include "Point.h"

class Cercle {
private:
    Point centre;
    int diametre;

public:
    Cercle(Point &centre, int diametre);

    Point getCentre() const;

    void setCentre(Point &point);

    int getDiametre() const;

    void setDiametre(int diametre);

    double calculerPerimetre() const;

    double calculerSurface() const;

    bool estSurLeCercle(Point &point) const;

    bool estDansLeCercle(Point &point) const;


};

#endif //TP2_CERCLE_H
