//
// Created by Fatih BOZLAK on 24/10/2023.
//

#ifndef TP2_CARRE_H
#define TP2_CARRE_H


#include "Rectangle.h"

class Carre : public Rectangle {
private:
    Point coinGauche;
public:
    Carre(int longueur, int largeur, Point &coinGauche, int longeur);
    Carre(int longueur, Point coinGauche);
};


#endif //TP2_CARRE_H
