# Analyse des projets
## Projet Tanguy
- Dans les classes héritant de Grilles, on trouve la logique permettant de déterminer la victoire des jeux. Cela est un manquement au SRP ainsi qu’au DIP.

- Les classes Héritant de jeu pourrait être regroupés dans un tableau de la classe mère afin de rendre l’affichage automatique et mieux adhérer au OCP

- Les classes héritant de Grilles et Pion sont en réalité des struct , cela pourrait ne pas respecter le principe d’encapsulation.

- L’utilisation d’un Pion unique pour le déroulement des parties pourrait limiter la modularité et donc enfreindre au OCP

- Les Classes héritant de Jeu utilise les objets grille et pion , violant ainsi la Loi de Demeter

- La méthode LancerJeu() des classes héritant de jeu comportent un nombre de boucles imbriqué bien trop élevé pour une bonne maintenabilité du code.

- Les classes héritant de participant contiennent une partie de la logique des jeu ce qui est un manquement au SRP et DIP

## Projet Fatih
- L’utilisation des shared_ptr peuvent être optimisés pour éviter les erreurs lors des sorties de boucles
  - MAJ : ce n'était pas un problème de pointeur, mais d'un attribut static (```nextId```)

- Les classes Héritant de jeu pourrait être regroupés dans un tableau de la classe mère afin de rendre l’affichage automatique et mieux adhérer au OCP

- Les affichages des grilles étant trop spécifique cela invalide l’OCP ainsi que la réutilisabilité du code

- L’ordre des attributs des classes ne respecte pas la norme du plus au moins accessible.

- Un travail est à fournir pour s’assurer que les méthodes permettant de jouer dans les classes Joueurs et Jeu respectent bien le DIP ainsi que le SRP


(voir branch DEV pour suivre les commits)