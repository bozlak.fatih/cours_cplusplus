//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include "IaMorpion.h"
#include "Pion.h"

IaMorpion::IaMorpion(std::string pseudo, char symbole) :
        Joueur(std::move(pseudo), symbole) {}

std::shared_ptr<IPion> IaMorpion::jouer() {
    srand(time(nullptr));

     // pas correcte, il faut pouvoir le range max, pour l'instant je ne sais pas ou pour le rendre génrique à la grille
    int ligneJouee = (rand() % 3);
    // pas correcte, il faut pouvoir le range max, pour l'instant je ne sais pas ou pour le rendre génrique à la grille
    int colonneJouee = (rand() % 3);

    // ce pion devra être vérifié par le jeu, sinon on redemande
    std::unique_ptr<IPion> pionJouee(std::make_unique<Pion>(colonneJouee, ligneJouee, this->getSymbole()));

    return pionJouee;
}