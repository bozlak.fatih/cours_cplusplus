//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include "Case.h"

#include <utility>

Case::Case(int x, int y, std::shared_ptr<IPion> contenu) :
        X(x),
        Y(y),
        contenu(std::move(contenu)) {}

int Case::getX() {
    return this->X;
}

int Case::getY() {
    return this->Y;
}

std::shared_ptr<IPion> Case::getContenu() {
    return this->contenu;
}