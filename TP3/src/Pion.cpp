//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include "Pion.h"

Pion::Pion(int x, int y, char symbole) :
        x(x),
        y(y),
        symbole(symbole) {}

std::array<int, 2> Pion::getPosition() {
    std::array<int, 2> position({this->x, this->y});

    return position;
}

char Pion::getSymbole() {
    return this->symbole;
}

std::shared_ptr<IJoueur> Pion::getJoueePar() {
    return this->joueePar;
}

void Pion::setJoueePar(std::shared_ptr<IJoueur> joueur) {
    this->joueePar = joueur;
}
