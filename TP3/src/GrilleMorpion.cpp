//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include "GrilleMorpion.h"
#include "Pion.h"

GrilleMorpion::GrilleMorpion(int nbLignes, int nbColonnes)
        : Grille(nbLignes, nbColonnes) {}

std::unique_ptr<std::pair<int, int>> GrilleMorpion::estEmplacementLibre(int nbLigne ,int nbColonne) {

    if ( grille[nbLigne][nbColonne]->getContenu()==nullptr)
    {
        return std::make_unique<std::pair<int, int>>(nbLigne , nbColonne);
    }

    return nullptr;
}

bool GrilleMorpion::estRemplie()
{
    for (int i = 0; i < nbLignes; i++) 
    {
        for (int j = 0; j < nbColonnes ; j++)
        {
            if (grille[i][j]->getContenu()==nullptr)
            {
                std::cout<<"case" <<j << i << "vide \n";
                return false;
            }
        }
    }
    return true;
}

void GrilleMorpion::initGrille()
{
    for (int i = 0; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes ; j++)
        {
           grille[i][j] == nullptr ;
        }
    }
}

// ChatGPT
std::shared_ptr<IJoueur> GrilleMorpion::checkAlignement() {

    // Vérifier les horizontales
    for (int i = 0; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes - 2; j++) { // -2 car nous avons besoin de place pour 3 pions alignés
            // Vérifiez que le premier pion de la séquence n'est pas null et récupérez son symbole
            auto premierPion = grille[i][j]->getContenu();
            if (premierPion && premierPion->getSymbole() != '\0') {
                char symbole = premierPion->getSymbole();

                // Vérifiez si les deux pions suivants ont le même symbole
                if (grille[i][j + 1]->getContenu() && grille[i][j + 1]->getContenu()->getSymbole() == symbole &&
                    grille[i][j + 2]->getContenu() && grille[i][j + 2]->getContenu()->getSymbole() == symbole ) {
                    return premierPion->getJoueePar(); // Trois pions alignés horizontalement trouvés
                }
            }
        }
    }

    // Vérifier les diagonales (bas-gauche vers haut-droit)
    for (int i = 3; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes - 2; j++) {
            auto premierPionDiagSec = grille[i][j]->getContenu();
            if (premierPionDiagSec && premierPionDiagSec->getSymbole() != '\0') {
                char symbole = premierPionDiagSec->getSymbole();
                if (grille[i - 1][j + 1]->getContenu() && grille[i - 1][j + 1]->getContenu()->getSymbole() == symbole &&
                    grille[i - 2][j + 2]->getContenu() && grille[i - 2][j + 2]->getContenu()->getSymbole() == symbole ) {
                    return premierPionDiagSec->getJoueePar(); // Trois pions alignés diagonalement (bas-gauche vers haut-droit)
                }
            }
        }
    }

    // Vérifier les diagonales (haut-gauche vers bas-droit)
    for (int i = 0; i < nbLignes - 3; i++) {
        for (int j = 0; j < nbColonnes - 2; j++) {
            auto premierPionDiagPrinc = grille[i][j]->getContenu();
            if (premierPionDiagPrinc && premierPionDiagPrinc->getSymbole() != '\0') {
                char symbole = premierPionDiagPrinc->getSymbole();
                if (grille[i + 1][j + 1]->getContenu() && grille[i + 1][j + 1]->getContenu()->getSymbole() == symbole &&
                    grille[i + 2][j + 2]->getContenu() && grille[i + 2][j + 2]->getContenu()->getSymbole() == symbole) {
                    return premierPionDiagPrinc->getJoueePar(); // Trois pions alignés diagonalement (haut-gauche vers bas-droit)
                }
            }
        }
    }

    // Vérifier les verticales
    for (int j = 0; j < nbColonnes; j++) {
        for (int i = 0; i < nbLignes - 2 ; i++) { // -2 pour laisser la place pour 3 pions alignés
            auto premierPion = grille[i][j]->getContenu();
            if (premierPion && premierPion->getSymbole() != '\0') {
                char symbole = premierPion->getSymbole();

                // Vérifiez si les trois pions suivants ont le même symbole
                if (grille[i + 1][j]->getContenu() && grille[i + 1][j]->getContenu()->getSymbole() == symbole &&
                    grille[i + 2][j]->getContenu() && grille[i + 2][j]->getContenu()->getSymbole() == symbole ) {
                    return premierPion->getJoueePar(); // Quatre pions alignés verticalement trouvés
                }
            }
        }
    }
    return nullptr; // Aucun alignement trouvé
}


std::shared_ptr<IPion> GrilleMorpion::poserPion(std::shared_ptr<IPion> pionAPoser) {

    int x = pionAPoser->getPosition()[0];
    int y = pionAPoser->getPosition()[1];

    if (estEmplacementLibre(x,y))
    {
        this->grille[x][y] = std::make_shared<Case>(x, y, std::make_shared<Pion>(y,x, pionAPoser->getSymbole()));
        return this->grille[x][y]->getContenu();
    }

    return nullptr;
}

void GrilleMorpion::afficherGrille() {
    for (int i = 0; i <= nbColonnes-1; i++) {
        std::cout << "  |   " << i << " ";
    }
    std::cout << "  |" << std::endl;
    std::cout << "  ---------------------------------------------------------" << std::endl;

    for (int i = 0; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes; j++) {
            if (grille[i][j]->getContenu() && grille[i][j]->getContenu()->getSymbole() != '\0')
                std::cout << "  |   " << grille[i][j]->getContenu()->getSymbole() << " ";
            else
                std::cout << "  |     ";
        }

        std::cout << "  |" << std::endl;
        std::cout << "  ---------------------------------------------------------" << std::endl;
    }
}