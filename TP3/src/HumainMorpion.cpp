//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include <utility>
#include "HumainMorpion.h"
#include "Pion.h"

HumainMorpion::HumainMorpion(std::string pseudo, char symbole) :
        Joueur(std::move(pseudo), symbole) {}

std::shared_ptr<IPion> HumainMorpion::jouer() {
    int ligneJouee = 0;
    int colonneJouee = 0;
    std::cin >> ligneJouee;
    std::cin >> colonneJouee;



    // ce pion devra être vérifié par le jeu, sinon on redemande
    std::shared_ptr<IPion> pionJouee(std::make_unique<Pion>(colonneJouee, ligneJouee, this->getSymbole()));

    return pionJouee;
}
