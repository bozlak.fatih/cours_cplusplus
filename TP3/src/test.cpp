//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include "Pion.h"
#include "Joueur.h"
#include "IaPuissance4.h"
#include "IaMorpion.h"
#include "GrillePuissance4.h"
#include "GrilleMorpion.h"
#include "Puissance4.h"
#include "Morpion.h"
#include "HumainPuissance4.h"
#include "HumainMorpion.h"

int main() {
//    // Pion
//    std::shared_ptr<IPion> p1 = std::make_shared<Pion>(Pion(10, 20, 'X'));
//    std::cout << p1->getSymbole() << std::endl;
//    std::cout << "(" << p1->getPosition()[0] << ", " << p1->getPosition()[1] << ")" << std::endl;
//    //TODO: rajouter des tests pour setJoueePar et getJoueePar
//
//    GrillePuissance4 grilleP = GrillePuissance4(4, 7);
//
//    grilleP.afficherGrille();
//
//    grilleP.poserPion(std::make_shared<Pion>(1, 0, 'X'));
//    grilleP.poserPion(std::make_shared<Pion>(2, 0, 'O'));
//    grilleP.poserPion(std::make_shared<Pion>(3, 0, 'X'));
//    grilleP.poserPion(std::make_shared<Pion>(4, 0, 'X'));
//    grilleP.poserPion(std::make_shared<Pion>(4, 0, 'X'));
//    grilleP.poserPion(std::make_shared<Pion>(5, 0, 'A'));
//    grilleP.poserPion(std::make_shared<Pion>(5, 0, 'A'));
//    grilleP.poserPion(std::make_shared<Pion>(5, 0, 'A'));
//    grilleP.poserPion(std::make_shared<Pion>(6, 0, 'A'));
//    grilleP.poserPion(std::make_shared<Pion>(6, 0, 'X'));
//    grilleP.poserPion(std::make_shared<Pion>(6, 0, 'A'));
//    grilleP.poserPion(std::make_shared<Pion>(6, 0, 'A'));
//    grilleP.poserPion(std::make_shared<Pion>(7, 0, 'X'));
//    grilleP.poserPion(std::make_shared<Pion>(4, 0, 'X'));
//    grilleP.poserPion(std::make_shared<Pion>(4, 0, 'X'));
//
//    if(grilleP.checkAlignement()) {
//        std::cout << "Alignement" << std::endl;
//    }
//
//    grilleP.afficherGrille();

    Morpion p = Morpion(3, 3);

    std::shared_ptr<IJoueur> j = std::make_shared<HumainMorpion>(HumainMorpion("fatih", 'X'));
    std::shared_ptr<IJoueur> jj = std::make_shared<HumainMorpion>(HumainMorpion("boby", '0'));

    p.ajouterJoueur(j);
    p.ajouterJoueur(jj);

    p.dirigerLaPartie(p.getJoueurs());

    return 0;
}