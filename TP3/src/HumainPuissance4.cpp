//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include <utility>
#include "HumainPuissance4.h"
#include "Pion.h"

HumainPuissance4::HumainPuissance4(std::string pseudo, char symbole) :
        Joueur(std::move(pseudo), symbole) {}

std::shared_ptr<IPion> HumainPuissance4::jouer() {
    int colonneJouee = 0;
    std::cin >> colonneJouee;

    // ce pion devra être vérifié par le jeu, sinon on redemande
    std::shared_ptr<IPion> pionJouee(std::make_unique<Pion>(colonneJouee, 0, this->getSymbole()));

    return pionJouee;
}
