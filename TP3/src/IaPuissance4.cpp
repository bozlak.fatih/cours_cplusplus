//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include "IaPuissance4.h"
#include "Pion.h"

IaPuissance4::IaPuissance4(std::string pseudo, char symbole) :
        Joueur(std::move(pseudo), symbole) {}

std::shared_ptr<IPion> IaPuissance4::jouer() {
    srand(time(nullptr));

    // pas correcte, il faut pouvoir le range max, pour l'instant je ne sais pas ou pour le rendre génrique à la grille
    int colonneJouee = 1 + (rand() % 7);

    // ce pion devra être vérifié par le jeu, sinon on redemande
    std::unique_ptr<IPion> pionJouee(std::make_unique<Pion>(colonneJouee, 0, this->getSymbole()));

    return pionJouee;
}