//
// Created by Fatih BOZLAK on 21/11/2023.
//


#include "Jeux.h"

#include <utility>

Jeux::Jeux(std::string nomDuJeu) :
        nomDuJeu(std::move(nomDuJeu)) {}

std::string Jeux::getNomDuJeu() {
    return this->nomDuJeu;
}

void Jeux::ajouterJoueur(std::shared_ptr<IJoueur> joueur) {
    this->joueurs.push_back(joueur);
}

std::vector<std::shared_ptr<IJoueur>> Jeux::getJoueurs() {
    return this->joueurs;
}