//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include <utility>
#include "Puissance4.h"
#include "IaPuissance4.h"

Puissance4::Puissance4(int nbLignes, int nbColonnes) :
        Jeux("Puissance4"),
        GrillePuissance4(nbLignes, nbColonnes) {}

// TODO: implémenter initialiserGrille

std::shared_ptr<IJoueur> Puissance4::quiGagne() {
    std::shared_ptr<IJoueur> vainqueur = checkAlignement();
    if (vainqueur != nullptr) {
        return vainqueur;
    }

    return nullptr;
}

void Puissance4::dirigerLaPartie(std::vector<std::shared_ptr<IJoueur>> joueurs) {
    this->afficherGrille();

    std::shared_ptr<IPion> pionValidePosee;
    std::shared_ptr<IPion> pionJoueeParJoueur = nullptr;
    std::shared_ptr<IJoueur> vainqueur = nullptr;

    while (true) {
        for (std::shared_ptr<IJoueur> j: joueurs) {
            pionValidePosee = nullptr;

            while (pionValidePosee == nullptr) {
                // c'est pas optimal du tout mais j'ai plus le temps ....
                auto iaPuissance4 = std::dynamic_pointer_cast<IaPuissance4>(j);
                if (!iaPuissance4)
                    std::cout << "A toi de jouer " << j->getPseudo() << " : ";

                pionJoueeParJoueur = j->jouer();

                pionValidePosee = this->poserPion(pionJoueeParJoueur);
                if (pionValidePosee != nullptr) {
                    pionValidePosee->setJoueePar(j);
                    break;
                }

                if (!iaPuissance4)
                    std::cout << "Mauvais choix ! (" << j->getPseudo() << ")" << std::endl;
            }

            std::shared_ptr<IJoueur> vainqueur = quiGagne();
            if (vainqueur) {
                std::cout << "Felicitation à " << vainqueur->getPseudo() << std::endl;
                this->afficherGrille();

                return;
            }

            this->afficherGrille();
        }
    }
}