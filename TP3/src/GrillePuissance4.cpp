//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include "GrillePuissance4.h"
#include "Pion.h"

GrillePuissance4::GrillePuissance4(int nbLignes, int nbColonnes)
        : Grille(nbLignes, nbColonnes) {}

std::unique_ptr<std::pair<int, int>> GrillePuissance4::checkEmplacementsLibresDansColonne(int nbColonne) {
    if (nbColonne >= this->nbColonnes) {
        return nullptr;
    }

    std::pair<int, int> potentielCaseLibre(-1, -1);

    int nbLigne = 0;
    for (auto ligne: this->grille) {
        if (ligne[nbColonne]->getContenu() == nullptr) {
            potentielCaseLibre.first = nbLigne;
            potentielCaseLibre.second = nbColonne;
        }

        nbLigne++;
    }

    if (potentielCaseLibre.second == -1)
        return nullptr;

    return std::make_unique<std::pair<int, int>>(potentielCaseLibre);
}

// ChatGPT
std::shared_ptr<IJoueur> GrillePuissance4::checkAlignement() {
    // Vérifier les horizontales
    for (int i = 0; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes - 3; j++) { // -3 car nous avons besoin de place pour 4 pions alignés
            // Vérifiez que le premier pion de la séquence n'est pas null et récupérez son symbole
            auto premierPion = grille[i][j]->getContenu();
            if (premierPion && premierPion->getSymbole() != '\0') {
                char symbole = premierPion->getSymbole();

                // Vérifiez si les trois pions suivants ont le même symbole
                if (grille[i][j + 1]->getContenu() && grille[i][j + 1]->getContenu()->getSymbole() == symbole &&
                    grille[i][j + 2]->getContenu() && grille[i][j + 2]->getContenu()->getSymbole() == symbole &&
                    grille[i][j + 3]->getContenu() && grille[i][j + 3]->getContenu()->getSymbole() == symbole) {
                    return premierPion->getJoueePar(); // Quatre pions alignés horizontalement trouvés
                }
            }
        }
    }

    // Vérifier les diagonales (bas-gauche vers haut-droit)
    for (int i = 3; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes - 3; j++) {
            auto premierPionDiagSec = grille[i][j]->getContenu();
            if (premierPionDiagSec && premierPionDiagSec->getSymbole() != '\0') {
                char symbole = premierPionDiagSec->getSymbole();
                if (grille[i - 1][j + 1]->getContenu() && grille[i - 1][j + 1]->getContenu()->getSymbole() == symbole &&
                    grille[i - 2][j + 2]->getContenu() && grille[i - 2][j + 2]->getContenu()->getSymbole() == symbole &&
                    grille[i - 3][j + 3]->getContenu() && grille[i - 3][j + 3]->getContenu()->getSymbole() == symbole) {
                    return premierPionDiagSec->getJoueePar(); // Quatre pions alignés diagonalement (bas-gauche vers haut-droit)
                }
            }
        }
    }

    // Vérifier les diagonales (haut-gauche vers bas-droit)
    for (int i = 0; i < nbLignes - 3; i++) {
        for (int j = 0; j < nbColonnes - 3; j++) {
            auto premierPionDiagPrinc = grille[i][j]->getContenu();
            if (premierPionDiagPrinc && premierPionDiagPrinc->getSymbole() != '\0') {
                char symbole = premierPionDiagPrinc->getSymbole();
                if (grille[i + 1][j + 1]->getContenu() && grille[i + 1][j + 1]->getContenu()->getSymbole() == symbole &&
                    grille[i + 2][j + 2]->getContenu() && grille[i + 2][j + 2]->getContenu()->getSymbole() == symbole &&
                    grille[i + 3][j + 3]->getContenu() && grille[i + 3][j + 3]->getContenu()->getSymbole() == symbole) {
                    return premierPionDiagPrinc->getJoueePar(); // Quatre pions alignés diagonalement (haut-gauche vers bas-droit)
                }
            }
        }
    }

    // Vérifier les verticales
    for (int j = 0; j < nbColonnes; j++) {
        for (int i = 0; i < nbLignes - 3; i++) { // -3 pour laisser la place pour 4 pions alignés
            auto premierPion = grille[i][j]->getContenu();
            if (premierPion && premierPion->getSymbole() != '\0') {
                char symbole = premierPion->getSymbole();

                // Vérifiez si les trois pions suivants ont le même symbole
                if (grille[i + 1][j]->getContenu() && grille[i + 1][j]->getContenu()->getSymbole() == symbole &&
                    grille[i + 2][j]->getContenu() && grille[i + 2][j]->getContenu()->getSymbole() == symbole &&
                    grille[i + 3][j]->getContenu() && grille[i + 3][j]->getContenu()->getSymbole() == symbole) {
                    return premierPion->getJoueePar(); // Quatre pions alignés verticalement trouvés
                }
            }
        }
    }

    return nullptr; // Aucun alignement trouvé
}


std::shared_ptr<IPion> GrillePuissance4::poserPion(std::shared_ptr<IPion> pionAPoser) {
    int colonneOuPoser = pionAPoser->getPosition()[0]; // coordonnée X = la colonne ou poser
    colonneOuPoser--; // aligner au formalisme des tableaux

    std::unique_ptr<std::pair<int, int>> emplacementLibre = checkEmplacementsLibresDansColonne(colonneOuPoser);
    if (emplacementLibre) {
        int x = emplacementLibre->first;
        int y = emplacementLibre->second;

        this->grille[x][y] = std::make_shared<Case>(x, y, std::make_shared<Pion>(x, y, pionAPoser->getSymbole()));

        return this->grille[x][y]->getContenu();
    }

    return nullptr;
}

void GrillePuissance4::afficherGrille() {
    for (int i = 1; i <= nbColonnes; i++) {
        std::cout << "  |   " << i << " ";
    }
    std::cout << "  |" << std::endl;
    std::cout << "  ---------------------------------------------------------" << std::endl;

    for (int i = 0; i < nbLignes; i++) {
        for (int j = 0; j < nbColonnes; j++) {
            if (grille[i][j]->getContenu() && grille[i][j]->getContenu()->getSymbole() != '\0')
                std::cout << "  |   " << grille[i][j]->getContenu()->getSymbole() << " ";
            else
                std::cout << "  |     ";
        }

        std::cout << "  |" << std::endl;
        std::cout << "  ---------------------------------------------------------" << std::endl;
    }

    std::cout << "" << std::endl;
}