#include <iostream>
#include "Jeux.h"
#include "Puissance4.h"
#include "HumainPuissance4.h"
#include "IaPuissance4.h"
#include "Morpion.h"
#include "HumainMorpion.h"
#include "IaMorpion.h"

int main() {
    std::vector<std::unique_ptr<Jeux>> jeux;
    std::vector<std::shared_ptr<IJoueur>> joueurs;

    jeux.push_back(std::make_unique<Puissance4>(4, 7));
    jeux.push_back(std::make_unique<Morpion>(3, 3));

    while (true) {
        int jeuxChoisi = -1;


        for (int i = 0; i < jeux.size(); i++) {
            std::cout << i+1 << ". " << jeux[i]->getNomDuJeu() << std::endl;
        }

        std::cout << "Quel jeux ? ";
        std::cin >> jeuxChoisi;

//        if (jeuxChoisi < jeux.size() || jeuxChoisi > jeux.size()) {
//            continue;
//        }

        jeuxChoisi--;

        Puissance4* puissance4Ptr = dynamic_cast<Puissance4*>(jeux[jeuxChoisi].get());
        if (puissance4Ptr) {
            joueurs.push_back(std::make_shared<HumainPuissance4>("Fatih", 'X'));
            joueurs.push_back(std::make_shared<IaPuissance4>("SuperIA", 'O'));
        }

        Morpion* morpionPtr = dynamic_cast<Morpion*>(jeux[jeuxChoisi].get());
        if (morpionPtr) {
            joueurs.push_back(std::make_shared<HumainMorpion>("Fatih", 'X'));
            joueurs.push_back(std::make_shared<IaMorpion>("SuperIA", 'O'));
        }

        jeux[jeuxChoisi]->dirigerLaPartie(joueurs);
    }
}
