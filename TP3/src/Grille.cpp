//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include "Grille.h"

Grille::Grille(int nbLignes, int nbColonnes) :
        grille(nbLignes, std::vector<std::shared_ptr<Case>>(nbColonnes)),
        nbLignes(nbLignes),
        nbColonnes(nbColonnes) {

    for (int i = 0; i < this->nbLignes; i++)
        for (int j = 0; j < this->nbColonnes; j++)
            this->grille[i][j] = std::make_shared<Case>(i, j, nullptr);
}

std::vector<std::shared_ptr<Case>> Grille::getEmplacementsLibres() {
    std::vector<std::shared_ptr<Case>> emplacementsLibres;

    for (int i = 0; i < this->nbLignes; i++) {
        for (int j = 0; j < this->nbLignes; j++) {
            if (this->grille[i][j] == nullptr) {
                emplacementsLibres.push_back(this->grille[i][j]);
            }
        }
    }

    return emplacementsLibres;
}

std::vector<std::shared_ptr<Case>> Grille::getEmplacementsPris() {
    std::vector<std::shared_ptr<Case>> emplacementPris;

    for (int i = 0; i < this->nbLignes; i++) {
        for (int j = 0; j < this->nbLignes; j++) {
            if (this->grille[i][j] != nullptr) {
                emplacementPris.push_back(this->grille[i][j]);
            }
        }
    }

    return emplacementPris;
}