//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include "Joueur.h"

#include <utility>

Joueur::Joueur(std::string pseudo, char symbole) :
        pseudo(std::move(pseudo)),
        symbole(symbole) {}

std::string Joueur::getPseudo() {
    return this->pseudo;
}

char Joueur::getSymbole() {
    return this->symbole;
}
