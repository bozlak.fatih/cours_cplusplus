//
// Created by Fatih BOZLAK on 21/11/2023.
//

#include <iostream>
#include <utility>
#include "Morpion.h"
#include "IaMorpion.h"

Morpion::Morpion(int nbLignes, int nbColonnes) :
        Jeux("Morpion"),
        GrilleMorpion(nbLignes, nbColonnes) {}

// TODO: implémenter initialiserGrille

std::shared_ptr<IJoueur> Morpion::quiGagne() {
    std::shared_ptr<IJoueur> vainqueur = checkAlignement();
    if (vainqueur != nullptr) {
        return vainqueur;
    }

    return nullptr;
}

void Morpion::dirigerLaPartie(std::vector<std::shared_ptr<IJoueur>> joueurs) {
    this->initGrille();
    this->afficherGrille();

    std::shared_ptr<IPion> pionValidePosee;
    std::shared_ptr<IPion> pionJoueeParJoueur = nullptr;
    std::shared_ptr<IJoueur> vainqueur = nullptr;

    while (true) {
        for (std::shared_ptr<IJoueur> j: joueurs) {
            pionValidePosee = nullptr;

            while (pionValidePosee == nullptr) {
                // c'est pas optimal du tout mais j'ai plus le temps ....
                auto iaMorpion = std::dynamic_pointer_cast<IaMorpion>(j);
                if (!iaMorpion)
                    std::cout << "A toi de jouer " << j->getPseudo() << " : ";

                pionJoueeParJoueur = j->jouer();

                pionValidePosee = this->poserPion(pionJoueeParJoueur);
                if (pionValidePosee != nullptr) {
                    pionValidePosee->setJoueePar(j);
                    break;
                }

                std::cout << "Mauvais choix ! (" << j->getPseudo() << ")" << std::endl;
            }

            std::shared_ptr<IJoueur> vainqueur = quiGagne();
            if (vainqueur) {
                std::cout << "Felicitation à " << vainqueur->getPseudo() << std::endl;
                return;
            }
            bool estNulle = this->estRemplie();
            if(estNulle)
            {
                std::cout << "Egalité !" << std::endl;
                return;
            }

            this->afficherGrille();
        }
    }
}