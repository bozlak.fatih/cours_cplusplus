//
// Created by Fatih BOZLAK on 20/11/2023.
//

/**
 * Idem à Humain
 */

#ifndef TP3_IA_H
#define TP3_IA_H

#include "Joueur.h"
#include "IPion.h"

class Ia : public Joueur {
public:
    Ia(std::string pseudo, char symbole);

    virtual std::shared_ptr<IPion> jouer() = 0;
};

#endif //TP3_IA_H
