#ifndef TP3_GRILLEMorpion_H
#define TP3_GRILLEMorpion_H

#include "Grille.h"

class GrilleMorpion : public Grille {
public:
    GrilleMorpion(int nbLignes, int nbColonnes);

    std::unique_ptr<std::pair<int, int>> estEmplacementLibre(int nbLigne ,int nbColonne);

    std::shared_ptr<IJoueur> checkAlignement();

    std::shared_ptr<IPion> poserPion(std::shared_ptr<IPion> pionAPoser) override;

    void afficherGrille() override;

    bool estRemplie();

    void initGrille();
};

#endif //TP3_GRILLEMorpion_H
