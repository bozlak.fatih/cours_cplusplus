//
// Created by Fatih BOZLAK on 21/11/2023.
//

/**
 * 1. Est-ce que ma classe à une seule responsibilité ?
 * Oui elle ne s'ocupe que de la gestiond d'un pion.
 *
 * 2. Ma classe est-elle ouverte à l'extension, mais fermée à la modification ?
 * Il sera possible de proposer des pions différents en heritant de cette classe.
 *
 * 3. Les objets de ma classe peuvent-ils être remplacés par des instances de leurs sous-classes sans affecter le comportement attendu ?
 * Oui
 *
 * 4. Est-ce que ma classe implémente des interfaces qu'elle n'utilise pas ?
 * Non elle n'utilise que IPion
 *
 * 5. Ma classe dépend-elle d'abstractions plutôt que de classes concrètes ?
 * Pas de classe concrête mais IJoueur
 */

#ifndef TP3_PION_H
#define TP3_PION_H

#include "IPion.h"

class Pion : public IPion {
public:
    Pion(int x, int y, char symbole);

    std::array<int, 2> getPosition() override;

    char getSymbole() override;

    std::shared_ptr<IJoueur> getJoueePar() override;

    void setJoueePar(std::shared_ptr<IJoueur>) override;

private:
    int x;
    int y;
    char symbole;
    std::shared_ptr<IJoueur> joueePar;
};

#endif //TP3_PION_H
