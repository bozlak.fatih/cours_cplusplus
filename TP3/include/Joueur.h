//
// Created by Fatih BOZLAK on 20/11/2023.
//

/**
 * 1. Est-ce que ma classe à une seule responsibilité ?
 * Oui cette classe à comme responsabilité la gestion d'un joueur.
 *
 * 2. Ma classe est-elle ouverte à l'extension, mais fermée à la modification ?
 * Oui, s'il est nécessaire d'avoir un comportement différent, il suffira d'hériter de cette classe,
 * sans toucher à cette classe.
 *
 * 3. Les objets de ma classe peuvent-ils être remplacés par des instances de leurs sous-classes sans affecter le comportement attendu ?
 * Oui, il sera possible de remplacer Joueur par un Humain, une IA en hértitant.
 *
 * 4. Est-ce que ma classe implémente des interfaces qu'elle n'utilise pas ?
 * Elle implémente une interface qu'elle doit obligatoirement utiliser.
 *
 * 5. Ma classe dépend-elle d'abstractions plutôt que de classes concrètes ?
 * Elle dépend de IPion qui est une interface.
 */

#ifndef TP3_JOUEUR_H
#define TP3_JOUEUR_H

#include <string>
#include <memory>
#include "IJoueur.h"
#include "IPion.h"

class Joueur : public IJoueur {
public:
    Joueur(std::string pseudo, char symbole);

    std::string getPseudo() override;

    /**
     * Pas du tout sûr que le symbole d'un Pion soit défini par le joueur.
     * J'aurai plus tendance à mettre ça directement dans un pion
     */
    char getSymbole() override;

private:
    std::string pseudo;
    char symbole;
};

#endif //TP3_JOUEUR_H
