//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_IPION_H
#define TP3_IPION_H

#include <array>
#include <memory>
#include "IJoueur.h"

class IJoueur;

class IPion {
public:
    virtual std::array<int, 2> getPosition() = 0;

    virtual char getSymbole() = 0;

    virtual std::shared_ptr<IJoueur> getJoueePar() = 0;

    virtual void setJoueePar(std::shared_ptr<IJoueur>) = 0;
};

#endif //TP3_IPION_H
