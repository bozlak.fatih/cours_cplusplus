//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_IAMORPION_H
#define TP3_IAMORPION_H

#include "Joueur.h"

class IaMorpion : public Joueur {
public:
    IaMorpion(std::string pseudo, char symbole);

    std::shared_ptr <IPion> jouer() override;
};

#endif //TP3_IAMORPION_H
