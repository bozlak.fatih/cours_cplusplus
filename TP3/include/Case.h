//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_CASE_H
#define TP3_CASE_H

#include <memory>
#include "IPion.h"

class Case {
public:
    Case(int x, int y, std::shared_ptr<IPion> contenu);

    int getX();

    int getY();

    std::shared_ptr<IPion> getContenu();

private:
    int X;
    int Y;

    std::shared_ptr<IPion> contenu;
};

#endif //TP3_CASE_H
