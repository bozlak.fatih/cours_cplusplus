//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_GRILLE_H
#define TP3_GRILLE_H

#include <vector>
#include <memory>
#include "IPion.h"
#include "Case.h"

class Grille {
public:
    Grille(int nbLignes, int nbColonnes);

    std::vector<std::shared_ptr<Case>> getEmplacementsLibres();

    std::vector<std::shared_ptr<Case>> getEmplacementsPris();

    virtual std::shared_ptr<IPion> poserPion(std::shared_ptr<IPion> pionAPoser) = 0;

    virtual void afficherGrille() = 0;

protected:
    int nbLignes;
    int nbColonnes;

    std::vector<std::vector<std::shared_ptr<Case>>> grille;

private:
};

#endif //TP3_GRILLE_H
