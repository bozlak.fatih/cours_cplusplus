//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_MORPION_H
#define TP3_MORPION_H

#include "Jeux.h"
#include "GrilleMorpion.h"

// Potentiellement trouver une interface pour GrilleMorpion
class Morpion : public Jeux, GrilleMorpion {
public:
    Morpion(int nbLignes, int nbColonnes);

    std::shared_ptr<IJoueur> quiGagne() override;

    void dirigerLaPartie(std::vector<std::shared_ptr<IJoueur>> joueurs) override;
};

#endif //TP3_MORPION_H
