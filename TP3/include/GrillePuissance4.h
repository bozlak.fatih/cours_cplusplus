//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_GRILLEPUISSANCE4_H
#define TP3_GRILLEPUISSANCE4_H

#include "Grille.h"

class GrillePuissance4 : public Grille {
public:
    GrillePuissance4(int nbLignes, int nbColonnes);

    std::unique_ptr<std::pair<int, int>> checkEmplacementsLibresDansColonne(int nbColonne);

    std::shared_ptr<IJoueur> checkAlignement();

    std::shared_ptr<IPion> poserPion(std::shared_ptr<IPion> pionAPoser) override;

    void afficherGrille() override;
};

#endif //TP3_GRILLEPUISSANCE4_H
