//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_HUMAINMORPION_H
#define TP3_HUMAINMORPION_H

#include "Joueur.h"
#include <memory>

class HumainMorpion : public Joueur {
public:
    HumainMorpion(std::string pseudo, char symbole);

    std::shared_ptr<IPion> jouer() override;
};

#endif //TP3_HUMAINMORPION_H
