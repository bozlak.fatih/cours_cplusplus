//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_PUISSANCE4_H
#define TP3_PUISSANCE4_H

#include "Jeux.h"
#include "GrillePuissance4.h"

// Potentiellement trouver une interface pour GrillePuissance4
class Puissance4 : public Jeux, GrillePuissance4 {
public:
    Puissance4(int nbLignes, int nbColonnes);

    std::shared_ptr<IJoueur> quiGagne() override;

    void dirigerLaPartie(std::vector<std::shared_ptr<IJoueur>> joueurs) override;
};

#endif //TP3_PUISSANCE4_H
