//
// Created by Fatih BOZLAK on 20/11/2023.
//

/**
 * 1. Est-ce que ma classe à une seule responsibilité ?
 * Oui car elle s'occupe du déroulement d'un jeu.
 *
 * 2. Ma classe est-elle ouverte à l'extension, mais fermée à la modification ?
 * Oui car en fonction des jeux, on ne touchera pas à cette classe, mais on rajoutera des fonctionnalitées en héritant de celle-ci.
 *
 * 3. Les objets de ma classe peuvent-ils être remplacés par des instances de leurs sous-classes sans affecter le comportement attendu ?
 * Oui, il sera parfaitement possible d'utiliser une sous classe de Jeux à la place de Jeux.
 *
 * 4. Est-ce que ma classe implémente des interfaces qu'elle n'utilise pas ?
 * Oui cette classe n'utilise pas d'interface qu'elle n'utilise pas.
 *
 * 5. Ma classe dépend-elle d'abstractions plutôt que de classes concrètes ?
 * Elle dépend de IJoueur qui est une interface et pas une classe concrête.
 * Cela veut dire que je pourrai changer ma classe Joueur, mais je suis sûr
 * de retrouver les fonctionnalitées obligatoire pour que mon Jeux ne casse pas,
 * car l'interface obligera le nouveau Joueur à implémenter ce qu'elle à besoin.
 */

#ifndef TP3_JEUX_H
#define TP3_JEUX_H

#include <memory>
#include <vector>
#include "IJoueur.h"

class Jeux {
public:
    Jeux(std::string nomDuJeu);

    std::string getNomDuJeu();

    void ajouterJoueur(std::shared_ptr<IJoueur> joueur);

    std::vector<std::shared_ptr<IJoueur>> getJoueurs();

    virtual std::shared_ptr<IJoueur> quiGagne() = 0;

    virtual void dirigerLaPartie(std::vector<std::shared_ptr<IJoueur>> joueurs) = 0;

private:
    std::string nomDuJeu;
    std::vector<std::shared_ptr<IJoueur>> joueurs;
};

#endif //TP3_JEUX_H
