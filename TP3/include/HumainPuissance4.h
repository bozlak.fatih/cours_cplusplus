//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_HUMAINPUISSANCE4_H
#define TP3_HUMAINPUISSANCE4_H

#include "Joueur.h"
#include <memory>

class HumainPuissance4 : public Joueur {
public:
    HumainPuissance4(std::string pseudo, char symbole);

    std::shared_ptr<IPion> jouer() override;
};

#endif //TP3_HUMAINPUISSANCE4_H
