//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_IJOUEUR_H
#define TP3_IJOUEUR_H

#include <string>
#include "IPion.h"

class IPion;

class IJoueur {
public:
    virtual std::string getPseudo() = 0;

    virtual char getSymbole() = 0;

    virtual std::shared_ptr<IPion> jouer() = 0;
};

#endif //TP3_IJOUEUR_H
