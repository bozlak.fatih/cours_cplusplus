//
// Created by Fatih BOZLAK on 21/11/2023.
//

#ifndef TP3_IAPUISSANCE4_H
#define TP3_IAPUISSANCE4_H

#include "Joueur.h"

class IaPuissance4 : public Joueur {
public:
    IaPuissance4(std::string pseudo, char symbole);

    std::shared_ptr <IPion> jouer() override;
};

#endif //TP3_IAPUISSANCE4_H
