//
// Created by Fatih BOZLAK on 20/11/2023.
//

/**
 * 1. Est-ce que ma classe à une seule responsibilité ?
 * Oui seulement la responsabilité d'un joueur humain
 *
 * 2. Ma classe est-elle ouverte à l'extension, mais fermée à la modification ?
 * Oui, en fonction d'un Jeux, il sera possible d'étendre cette classe pour implémenter la logique en question du Jeux,
 * par exemple, pour le morpion il sera possibel d'avoir une classe HumainMorpion qui implémente sa propre logique de
 * comment jouer.
 *
 * 3. Les objets de ma classe peuvent-ils être remplacés par des instances de leurs sous-classes sans affecter le comportement attendu ?
 * Oui, il sera possible de remplacer par exemple, Humain par HumainMorpion
 *
 * 4. Est-ce que ma classe implémente des interfaces qu'elle n'utilise pas ?
 * Non
 *
 * 5. Ma classe dépend-elle d'abstractions plutôt que de classes concrètes ?
 * Elle dépend de IPion qui est une interface.
 */

#ifndef TP3_HUMAIN_H
#define TP3_HUMAIN_H

#include "Joueur.h"
#include "IPion.h"

// TODO: Choisir la bonne classe "Joueur_vN.h" à implémenter en fonction des réponses du prof
class Humain : public Joueur {
public:
    Humain(std::string pseudo, char symbole);

    virtual std::shared_ptr<IPion> jouer() = 0;
};

#endif //TP3_HUMAIN_H
